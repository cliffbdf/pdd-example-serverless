package cliffberg.pddexample.serverlessclient;

public class RequestClass {

	public String BucketName;
	public String PrimaryFileKey;
	public String SecondaryFileKey;
	public String OutFileKey;

	public RequestClass(String bucketName, String primaryFileKey, String secondaryFileKey, String outFileKey) {
		this.BucketName = bucketName;
		this.PrimaryFileKey = primaryFileKey;
		this.SecondaryFileKey = secondaryFileKey;
		this.OutFileKey = outFileKey;
	}

	public RequestClass() {}

	public String getBucketName() { return BucketName; }
	public void setBucketName(String name) { this.BucketName = name; }

	public String getPrimaryFileKey() { return PrimaryFileKey; }
	public void setPrimaryFileKey(String key) { this.PrimaryFileKey = key; }

	public String getSecondaryFileKey() { return SecondaryFileKey; }
	public void setSecondaryFileKey(String key) { this.SecondaryFileKey = key; }

	public String getOutFileKey() { return OutFileKey; }
	public void setOutFileKey(String key) { this.OutFileKey = key; }
}
